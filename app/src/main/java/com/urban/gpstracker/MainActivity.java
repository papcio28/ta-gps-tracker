package com.urban.gpstracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NewGpsBroadcastReceiver.OnGpsListener {
    private GpsDatabase mGpsDatabase;

    @BindView(R.id.lista)
    protected RecyclerView mList;
    private NewGpsBroadcastReceiver gpsReceiver;
    private GpsEntryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mGpsDatabase = new GpsDatabase(this);
        if (mGpsDatabase.getAll().size() == 0) {
            GpsEntry gps = new GpsEntry();
            gps.setFetchDate(new Date());
            gps.setLocationDate(new Date());
            gps.setAccuracy(50.0);
            gps.setGpsLatitude(51.61200123);
            gps.setGpsLongitude(17.119085);
            gps.setProvider("fake");

            mGpsDatabase.save(gps);
        }

        mList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GpsEntryAdapter(mGpsDatabase.getAll());
        mList.setAdapter(adapter);

        startService(new Intent(this, GpsService.class));

        gpsReceiver = new NewGpsBroadcastReceiver();
        gpsReceiver.setListener(this);
        gpsReceiver.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(gpsReceiver);
    }

    @Override
    public void onGpsChanged() {
        adapter.setData(mGpsDatabase.getAll());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_map) {
            Intent mapintent = new Intent(this, MapsActivity.class);
            startActivity(mapintent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
