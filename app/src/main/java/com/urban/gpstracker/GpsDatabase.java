package com.urban.gpstracker;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import android.content.Context;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

public class GpsDatabase {
    private Dao<GpsEntry, Integer> mGpsDao;

    public GpsDatabase(Context context) {
        try {
            ConnectionSource cs = new AndroidConnectionSource(
                    new GpsTrackerDbOpenHelper(context));
            mGpsDao = DaoManager.createDao(cs, GpsEntry.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void save(GpsEntry entry) {
        try {
            mGpsDao.create(entry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<GpsEntry> getAll() {
        try {
            return mGpsDao.queryBuilder().orderBy("fetchDate", false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
