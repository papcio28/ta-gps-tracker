package com.urban.gpstracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class NewGpsBroadcastReceiver extends BroadcastReceiver {
    public static final String ACTION_NEW_GPS = BuildConfig.APPLICATION_ID + ".NEW_GPS";

    private OnGpsListener mListener;

    public void setListener(OnGpsListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (mListener != null) {
            mListener.onGpsChanged();
        }
    }

    public void register(Context context) {
        context.registerReceiver(this, new IntentFilter(ACTION_NEW_GPS));
    }

    public interface OnGpsListener {
        void onGpsChanged();
    }
}
