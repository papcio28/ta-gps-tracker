package com.urban.gpstracker;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GpsEntryAdapter extends RecyclerView.Adapter<GpsEntryAdapter.GpsEntryViewHolder> {
    private List<GpsEntry> mData;
    private DateFormat mDateFormat;

    public GpsEntryAdapter(List<GpsEntry> data) {
        mData = data;
        mDateFormat = DateFormat.getDateTimeInstance();
    }

    public void setData(List<GpsEntry> mData) {
        this.mData = mData;
    }

    @Override
    public GpsEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new GpsEntryViewHolder(inflater.inflate(R.layout.item_gps, parent, false));
    }

    @Override
    public void onBindViewHolder(GpsEntryViewHolder holder, int position) {
        GpsEntry item = mData.get(position);

        setText(holder.mFetchDate, item.getFetchDate());
        setText(holder.mLatitude, item.getGpsLatitude());
        setText(holder.mLongitude, item.getGpsLongitude());
        setText(holder.mAccuracy, item.getAccuracy());
        setText(holder.mProvider, item.getProvider());
        setText(holder.mGpsDate, item.getLocationDate());
    }

    private void setText(TextView target, Date date) {
        setText(target, date != null ? mDateFormat.format(date) : null);
    }

    private void setText(TextView target, Double aDouble) {
        setText(target, "Lat: " + (aDouble != null ? Double.toString(aDouble) : null));
    }

    private void setText(TextView target, String aString) {
        target.setText(aString != null ? aString : "-");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class GpsEntryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.latitude)
        protected TextView mLatitude;
        @BindView(R.id.longitude)
        protected TextView mLongitude;
        @BindView(R.id.accuracy)
        protected TextView mAccuracy;
        @BindView(R.id.gps_date)
        protected TextView mGpsDate;
        @BindView(R.id.provider)
        protected TextView mProvider;
        @BindView(R.id.fetch_date)
        protected TextView mFetchDate;

        public GpsEntryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}























