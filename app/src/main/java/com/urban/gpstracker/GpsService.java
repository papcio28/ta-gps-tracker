package com.urban.gpstracker;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;

import java.util.Date;

public class GpsService extends IntentService implements LocationListener {
    private GpsDatabase mGpsDatabase;
    private LocationManager mLocationManager;
    private AlarmManager mAlarmManager;
    private HandlerThread mUpdatesThread;
    private GpsEntry mEntry;

    public GpsService() {
        super("GpsService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mEntry = new GpsEntry();
        mEntry.setFetchDate(new Date());
        mGpsDatabase = new GpsDatabase(this);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mUpdatesThread = new HandlerThread("GpsUpdateThread");
        mUpdatesThread.start();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // 1. Nasluchiwac na lokalizacje przez 1 minute
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000, 5, this, mUpdatesThread.getLooper());
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                1000, 5, this, mUpdatesThread.getLooper());
        try {
            Thread.sleep(60 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mLocationManager.removeUpdates(this);
        // 2. Pobrana lokalizacje zapisujemy w DB
        mGpsDatabase.save(mEntry);
        // 3. Wyslanie Broadcast z informacją o nowej lokalizacji
        Intent broadcastIntent = new Intent(NewGpsBroadcastReceiver.ACTION_NEW_GPS);
        sendBroadcast(broadcastIntent);
        // 4. Planujemy kolejne wykonanie usługi
        // TODO: Po zajęciach zwiększ interwał do 10 minut !
        Intent serviceIntent = new Intent(this, GpsService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 4500,
                serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + 3 * 60 * 1000, pendingIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUpdatesThread.quit();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mEntry.getAccuracy() == null || mEntry.getAccuracy() > location.getAccuracy()) {
            mEntry.setAccuracy((double) location.getAccuracy());
            mEntry.setProvider(location.getProvider());
            mEntry.setLocationDate(new Date(location.getTime()));
            mEntry.setGpsLatitude(location.getLatitude());
            mEntry.setGpsLongitude(location.getLongitude());
            mEntry.setFetchDate(new Date());
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
