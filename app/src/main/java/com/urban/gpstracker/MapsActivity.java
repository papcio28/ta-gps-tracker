package com.urban.gpstracker;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationChangeListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private GpsDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mDatabase = new GpsDatabase(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        LatLng markerPosition = new LatLng(51.20, 17.12);
        PolylineOptions polyline = new PolylineOptions();
        for (GpsEntry point : mDatabase.getAll()) {
            if (point.getGpsLatitude() == null || point.getGpsLongitude() == null) {
                continue;
            }
            markerPosition = new LatLng(point.getGpsLatitude(), point.getGpsLongitude());
            mMap.addMarker(new MarkerOptions().position(markerPosition)
                    .title(point.getProvider())
                    .snippet(point.getFetchDate().toString()));
            polyline.add(markerPosition);
            mMap.addCircle(new CircleOptions().center(markerPosition)
                    .radius(point.getAccuracy()));
        }
        mMap.addPolyline(polyline);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(markerPosition, 13));

        // Jednorazowy zoom do mojej pozycji - po znalezieniu !
        mMap.setOnMyLocationChangeListener(this);

        // Wyslanie SMS na klikniecie dymku
        mMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onMyLocationChange(Location location) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()), 13));
        mMap.setOnMyLocationChangeListener(null);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String mapsLink = String.format(Locale.US, "https://www.google.com/maps?q=%f,%f&z=13",
                marker.getPosition().latitude, marker.getPosition().longitude);

        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:"));
        smsIntent.putExtra("sms_body", mapsLink);

        startActivity(smsIntent);
    }
}


















